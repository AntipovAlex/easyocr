package com.antipov.easyocr.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.antipov.easyocr.interfaces.IOnBitmapSave;
import com.antipov.easyocr.utils.Const;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by alex on 10.10.2017.
 */

public class SaveBitmap extends AsyncTask<Void, Void, String> {
    private final File storage;
    private final IOnBitmapSave listener;
    private final String mode;
    private Bitmap bmp;
    private String name = "thumb_"+((Long)System.currentTimeMillis()).toString();
    private String name2;

    public SaveBitmap(File storage, String mode, Bitmap bmp, IOnBitmapSave listener) {
        this.storage = storage;
        this.listener = listener;
        this.bmp = bmp;
        this.mode = mode;
    }

    @Override
    protected String doInBackground(Void... voids) {
        FileOutputStream fileOutputStream = null;
        try{
            Log.d("Test-scaling", "BEFORE");
            Log.d("Test-scaling", "Original : "+String.valueOf(bmp.getWidth()) +" "+ String.valueOf(bmp.getHeight()));
            if (mode.equals(Const.Mode.THUMBNAIL)){ // saving only thumb
                name += ".webp";
                File file = new File(storage, name);
                fileOutputStream = new FileOutputStream(file);
                bmp = Bitmap.createScaledBitmap(bmp, (int)(bmp.getWidth()*0.5), (int)(bmp.getHeight()*0.5), true);
                bmp.compress(Bitmap.CompressFormat.WEBP, 5, fileOutputStream);
            } else if (mode.equals(Const.Mode.FULLIMAGE)) { // saving only full
                name += ".png";
                File file = new File(storage, name);
                fileOutputStream = new FileOutputStream(file);
                Log.d("Test-scaling", String.valueOf(bmp.getWidth()) + String.valueOf(bmp.getHeight()));
                bmp.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            } else if (mode.equals(Const.Mode.THUMBANDFULL)){ // saving both
                Log.d("Test-scaling", "THUMBANDFULL MODE");
                name += ".webp";
                File file = new File(storage, name);
                fileOutputStream = new FileOutputStream(file);
                Bitmap bmpThumb = Bitmap.createScaledBitmap(bmp, (int)(bmp.getWidth()*0.5), (int)(bmp.getHeight()*0.5), true);
                Log.d("Test-scaling", "Thumbnail "+String.valueOf(bmpThumb.getWidth()) +" "+ String.valueOf(bmpThumb.getHeight()));
                bmpThumb.compress(Bitmap.CompressFormat.WEBP, 5, fileOutputStream);
                name2 = "full"+((Long)System.currentTimeMillis()).toString();
                name2 += ".jpeg";
                File file2 = new File(storage, name2);
                fileOutputStream = new FileOutputStream(file2);
                Log.d("Test-scaling", "Compressed full: "+ String.valueOf(bmp.getWidth()) +" "+ String.valueOf(bmp.getHeight()));
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return storage.getAbsolutePath()+"/"+name;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (mode.equals(Const.Mode.THUMBANDFULL)){
            listener.onBitmapSaveThumbAndFullSuccess(storage.getAbsolutePath()+"/"+name2, storage.getAbsolutePath()+"/"+name);
        } else {
            listener.onBitmapSaveSuccess(s);
        }
    }
}

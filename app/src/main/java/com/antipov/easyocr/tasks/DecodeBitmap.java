package com.antipov.easyocr.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by alex on 10.10.2017.
 */

public class DecodeBitmap extends AsyncTask<File, Void, Bitmap> {

    @Override
    protected Bitmap doInBackground(File... files) {
        try {
            return BitmapFactory.decodeStream(new FileInputStream(files[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
    }
}

package com.antipov.easyocr.utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.antipov.easyocr.interfaces.IOnBitmapSave;
import com.antipov.easyocr.tasks.SaveBitmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by alex on 09.10.2017.
 */

public class ImageUtils implements IOnBitmapSave {

    public static final int PICK_CAMERA = 10;
    public static final int PICK_GALLERY = 11;
    private final Activity activity;
    private final IOnBitmapSave listener;
    private Uri imageUri;
    private final String TEMP_FILE_NAME = "TEMP_PIC.jpg";
    private ContextWrapper contextWrapper;
    private File storage;

    public ImageUtils(Activity activity, IOnBitmapSave listener) {
        this.activity = activity;
        this.contextWrapper = new ContextWrapper(activity.getApplicationContext());
        this.storage = contextWrapper.getDir("images", Context.MODE_PRIVATE);
        this.listener = listener;
    }

    public void pickFromCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(),  TEMP_FILE_NAME);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        intent.putExtra("uri_test", imageUri);
        imageUri = Uri.fromFile(photo);
        activity.startActivityForResult(intent, PICK_CAMERA);
    }

    public void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        activity.startActivityForResult(intent, PICK_GALLERY);
    }

    public Uri getImageUri(){
        return imageUri;
    }

    public void savePicture(Uri uri, String mode){
        try {
            this.imageUri = uri;
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imageUri);
            SaveBitmap saveBitmap = new SaveBitmap(storage, mode, bitmap, this);
            saveBitmap.execute();
        } catch (Throwable t) {
            t.printStackTrace();
            onBitmapSaveError(t.getMessage());
        }
    }

    @Override
    public void onBitmapSaveSuccess(String path) {
        listener.onBitmapSaveSuccess(path);
    }

    @Override
    public void onBitmapSaveThumbAndFullSuccess(String full, String thumb) {
        listener.onBitmapSaveThumbAndFullSuccess(full, thumb);
    }

    @Override
    public void onBitmapSaveError(String error) {
        listener.onBitmapSaveError(error);
    }

    public static Bitmap getFromInternalStorage(String path){
        try {
            File file = new File(path);
            FileInputStream fileInputStream = new FileInputStream(file);
            BitmapFactory.Options options = new BitmapFactory.Options();
            // don't use transparency -- save some kbytes of memory
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            logMemory();
            Bitmap bitmap = BitmapFactory.decodeStream(fileInputStream, null, options);
            logMemory();
            fileInputStream.close();
            return bitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteTempFile(){
        File file = new File(Environment.getExternalStorageDirectory(),  TEMP_FILE_NAME);
        file.delete();
    }

    private static void logMemory() {
        Log.i("MEMORY_LOG", String.format("Total memory = %s",
                (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }

    public void deleteSavedFile(String path) {
        File file = new File(path);
        file.delete();
    }
}

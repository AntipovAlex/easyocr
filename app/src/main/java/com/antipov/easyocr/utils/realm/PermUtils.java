package com.antipov.easyocr.utils.realm;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by alex on 19.10.2017.
 */

public class PermUtils {
    public static boolean checkIfAlreadyhavePermission(Activity activity) {
        int camera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        if (camera == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
}

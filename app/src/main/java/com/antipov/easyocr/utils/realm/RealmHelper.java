package com.antipov.easyocr.utils.realm;

import android.util.Log;

import com.antipov.easyocr.model.realm.Document;
import com.antipov.easyocr.model.realm.DocumentFile;

import java.util.ArrayList;
import java.util.Collection;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by alex on 08.10.2017.
 */

public class RealmHelper {

    public static int addProject(final Document p){
        final int nextId[] = new int[1];
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // primary key AI implementation
                Number id = realm.where(Document.class).max("id");
                nextId[0] = (id != null) ? id.intValue() + 1 : 0;
                // persisting object
                p.setId(nextId[0]);
                realm.copyToRealm(p);
            }
        });
        return nextId[0];
    }

    public static void addFile(final DocumentFile f){
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // primary key AI implementation
                Number id = realm.where(DocumentFile.class).max("id");
                int nextId = (id != null) ? id.intValue() + 1 : 0;
                // persisting object
                f.setId(nextId);
                realm.copyToRealm(f);
            }
        });
    }

    public static void addFileToProject(final Integer projectId, final Integer fileId){
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Document document = realm.where(Document.class).equalTo("id", projectId).findFirst();
                DocumentFile file = realm.where(DocumentFile.class).equalTo("id", fileId).findFirst();
                document.addFile(file);
            }
        });
    }

    public static RealmResults<Document> getProjectList(){
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        return realm.where(Document.class).findAll();
    }

    public static Document getDocument(int id){
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        return realm.where(Document.class).equalTo("id", id).findFirst();
    }

    public static void logProject(RealmResults<Document> p){
        for (Document document : p){
            Log.d("PROJECT_DUMP", "===========================");
            Log.d("PROJECT_DUMP", "id "+ document.getId());
            Log.d("PROJECT_DUMP", "name "+ document.getName());
//            Log.d("PROJECT_DUMP", "===========================");
//            Log.d("PROJECT_DUMP", "===========================");
//            Log.d("PROJECT_DUMP", "===========================");
        }
    }

    public static ArrayList<? extends RealmObject> extractList(RealmResults<? extends RealmObject> p){
        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
        return  (ArrayList<? extends RealmObject>) realm.copyFromRealm(p);
    }


//    public static ArrayList<DocumentFile> extractList(RealmResults<DocumentFile> p){
//        Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
//        return  (ArrayList<DocumentFile>) realm.copyFromRealm(p);
//    }
}

package com.antipov.easyocr.utils;

/**
 * Created by alex on 09.10.2017.
 */

public class Const {
    public static String BASE_URL_SERVER = "http://31.41.221.148:8080/easyocr/";
//    public static String BASE_URL_SERVER = "http://145.239.229.149:8080/easyocr/";
    public static String FLURRY_API_KEY = "JW7RSQ5QNNMFD9WZDZ5R";
    public static String ADMOB_APP_ID = "ca-app-pub-9329072439093171~2111280064";
    public static int RETRY_COUNT = 5;

    public static class Intent{
        public static final String PROJECT_ID = "projectid";
        public static final String PROJECT_NAME = "projectname";
        public static final String TEXT = "text";
        public static final String FILE_ID = "fileid";
    }

    public static class Mode{
        public static String THUMBNAIL = "thumbnail";
        public static String FULLIMAGE = "full";
        public static String THUMBANDFULL = "thumbandfull";
    }

    public static class Validation{
        public static final int MAX_LENGHT_TITLE = 50;
        public static final int MIN_LENGHT_TITLE = 5;
        public static final int MAX_LENGHT_DESCR = 250;
        public static final int MIN_LENGHT_DESCR = 5;
        public static final String REG_EXP_TITLE = "^.{"+MIN_LENGHT_TITLE+","+MAX_LENGHT_TITLE+"}$";
        public static final String REG_EXP_DESCR = "^.{"+MIN_LENGHT_DESCR+","+MAX_LENGHT_DESCR+"}$";
    }

    public static class ImageValues {
        public static int THUMBHEIGHT = 640;
        public static int THUMBWIDTH = 400;
    }
}

package com.antipov.easyocr.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alex on 15.10.2017.
 */

public class DateUtils {
    public static String getDate(){
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date());
    }
}

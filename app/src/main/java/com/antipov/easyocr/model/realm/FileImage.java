package com.antipov.easyocr.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by alex on 12.10.2017.
 */

public class FileImage extends RealmObject {
    @PrimaryKey
    private long id;
    private String filePath;

    public FileImage(String filePath) {
        this.filePath = filePath;
    }

    public FileImage() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}

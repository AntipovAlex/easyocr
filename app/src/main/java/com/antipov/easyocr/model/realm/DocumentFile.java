package com.antipov.easyocr.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by alex on 08.10.2017.
 */

public class DocumentFile extends RealmObject{
    @PrimaryKey
    private long id;
    private long imageId;
    private long documentId;
    private String image;
    private String text;

    public DocumentFile() {
    }

    public DocumentFile(String text, String i) {
        this.text = text;
        this.image = i;
    }

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

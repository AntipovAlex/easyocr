package com.antipov.easyocr.model.realm;

import com.antipov.easyocr.utils.DateUtils;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by alex on 08.10.2017.
 */

public class Document extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;
    private String description;
    private String coverPath;
    private String dateCreate;
    private RealmList<DocumentFile> files = new RealmList<>();

    public Document() {
        files = new RealmList<>();
    }

    public void addFile(DocumentFile file) {
        this.files.add(file);
    }

    public Document(String name, String description, String cover, RealmList<DocumentFile> files) {
        this.name = name;
        this.coverPath = cover;
        this.files = files;
        this.description = description;
        this.dateCreate = DateUtils.getDate();
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<DocumentFile> getFiles() {
        return files;
    }

    public void setFiles(RealmList<DocumentFile> files) {
        this.files = files;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }
}

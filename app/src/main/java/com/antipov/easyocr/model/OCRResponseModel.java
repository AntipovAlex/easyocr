package com.antipov.easyocr.model;

/**
 * Created by alex on 11.10.2017.
 */

public class OCRResponseModel {
    String status;
    String error;
    String data;

    public boolean isSuccessfull() {
        return status.equals("true");
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

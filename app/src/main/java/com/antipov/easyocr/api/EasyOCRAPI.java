package com.antipov.easyocr.api;

import com.antipov.easyocr.model.OCRResponseModel;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by alex on 11.10.2017.
 */

public interface EasyOCRAPI {
    @Multipart
    @POST("send")
    Observable<OCRResponseModel> upload(@Part MultipartBody.Part file, @Query("lang") String lang);
}

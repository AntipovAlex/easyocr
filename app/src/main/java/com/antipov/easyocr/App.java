package com.antipov.easyocr;

import android.app.Application;
import android.os.StrictMode;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.MobileAds;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static android.util.Log.VERBOSE;
import static com.antipov.easyocr.utils.Const.ADMOB_APP_ID;
import static com.antipov.easyocr.utils.Const.FLURRY_API_KEY;

/**
 * Created by alex on 08.10.2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // realm init
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        // flurry init
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withCaptureUncaughtExceptions(true)
                .withContinueSessionMillis(10)
                .withLogEnabled(true)
                .withLogLevel(VERBOSE)
                .build(this, FLURRY_API_KEY);

        MobileAds.initialize(this, ADMOB_APP_ID);
    }
}

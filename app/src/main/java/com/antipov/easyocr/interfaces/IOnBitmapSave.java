package com.antipov.easyocr.interfaces;

/**
 * Created by alex on 09.10.2017.
 */

public interface IOnBitmapSave {
    void onBitmapSaveSuccess(String path);

    void onBitmapSaveThumbAndFullSuccess(String full, String thumb);

    void onBitmapSaveError(String error);
}

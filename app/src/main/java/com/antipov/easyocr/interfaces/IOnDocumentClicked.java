package com.antipov.easyocr.interfaces;

/**
 * Created by alex on 10.10.2017.
 */

public interface IOnDocumentClicked {
    void onDocumentClicked(long id, String name);
}

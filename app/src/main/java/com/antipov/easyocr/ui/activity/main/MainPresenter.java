package com.antipov.easyocr.ui.activity.main;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 08.10.2017.
 */

public interface MainPresenter extends BasePresenter {
    void loadDocuments();

    void loadDocument(long id);
}

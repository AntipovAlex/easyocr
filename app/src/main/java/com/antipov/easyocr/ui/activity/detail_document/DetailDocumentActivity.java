package com.antipov.easyocr.ui.activity.detail_document;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.easyocr.R;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;

public class DetailDocumentActivity extends BaseActivity implements DetailDocumentView, DetailDocumentView.OnGetInfo,
DetailDocumentView.OnDeleteProject, View.OnClickListener {

    private long id;
    private DetailDocumentPresenterImpl mPresenter;
    private TextView mTitle;
    private TextView mCreatedAt;
    private TextView mDescription;
    private TextView mDocumentCount;
    private Button mDelete;
    private View mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.getInfo(id);
    }

    @Override
    protected void getExtras() {
        id = getIntent().getExtras().getLong(Const.Intent.PROJECT_ID);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail_document;
    }

    @Override
    protected void initViews() {
        mTitle = findViewById(R.id.tv_project_info);
        mCreatedAt = findViewById(R.id.tv_created);
        mDescription = findViewById(R.id.tv_description);
        mDocumentCount = findViewById(R.id.tv_files_count);
        mDelete = findViewById(R.id.btn_delete_project);
        mContainer = findViewById(R.id.ll_container);
    }

    @Override
    protected void initListeners() {
        mDelete.setOnClickListener(this);
        mContainer.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initPresenter() {
        mPresenter = new DetailDocumentPresenterImpl(this);
    }

    @Override
    public void onGetInfoResult(int documentCount, String createdAt, String description, String name) {
        mCreatedAt.setText(getString(R.string.created_at, createdAt));
        mTitle.setText(getString(R.string.project_title_info, name));
        mDescription.setText(getString(R.string.project_description_info, description));
        mDocumentCount.setText(getString(R.string.files_count, String.valueOf(documentCount)));
    }

    @Override
    public void onGetInfoError(String error) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_delete_project:
                DialogUtils.createBasicDialog(this, R.string.delete, getString(R.string.are_you_sure_delete_project), true, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showProgress();
                        mPresenter.deleteProject(id);
                    }
                }, getString(R.string.ok), getString(R.string.cancel)).show();

                break;
            case R.id.ll_container:
                finish();
                break;
        }
    }

    @Override
    public void onDeleteProjectResult(long id) {
        hideProgress();
        Intent intent = new Intent();
        intent.putExtra(Const.Intent.PROJECT_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onDeleteProjectError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.error_during_deleting_project, error));
    }

    @Override
    public void onNoInternet() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}

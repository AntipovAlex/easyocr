package com.antipov.easyocr.ui.activity.main;

import com.antipov.easyocr.model.realm.Document;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by alex on 08.10.2017.
 */

public class MainInteractorImpl implements MainInteractor {
    @Override
    public void loadDocuments(final OnLoadDocuments listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            RealmResults<Document> documents = realm.where(Document.class).findAllAsync();
            documents.addChangeListener(new RealmChangeListener<RealmResults<Document>>() {
                @Override
                public void onChange(RealmResults<Document> documents) {
                    listener.onLoadDocumentsResult(documents);
                    documents.removeAllChangeListeners();
                }
            });
        } catch (Exception e){
            listener.onLoadDocumentsError(e.getMessage());
        }
    }

    @Override
    public void loadDocument(long id, final OnLoadDocument listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            Document document = realm.where(Document.class).equalTo("id", id).findFirstAsync();
            document.addChangeListener(new RealmChangeListener<Document>() {
                @Override
                public void onChange(Document document) {
                    listener.onLoadDocumentResult(document);
                }
            });
        } catch (Exception e){
            listener.onLoadDocumentError(e.getMessage());
        }
    }


}

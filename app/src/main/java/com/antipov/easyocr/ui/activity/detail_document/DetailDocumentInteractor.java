package com.antipov.easyocr.ui.activity.detail_document;


/**
 * Created by alex on 15.10.2017.
 */

interface DetailDocumentInteractor {

    void getInfo(long id, OnGetInfo listener, OnNoInternet onNoInternet);

    void deleteProject(long id, OnDeleteProject listener);

    interface OnGetInfo {
        void onGetInfoResult(int documentCount, String createdAt, String description, String name);

        void onGetInfoError(String error);
    }

    interface OnDeleteProject {
        void onDeleteProjectResult(long id);

        void onDeleteProjectError(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}

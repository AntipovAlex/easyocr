package com.antipov.easyocr.ui.activity.detail_document;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 15.10.2017.
 */

public interface DetailDocumentPresenter extends BasePresenter{
    void getInfo(long id);

    void deleteProject(long id);
}

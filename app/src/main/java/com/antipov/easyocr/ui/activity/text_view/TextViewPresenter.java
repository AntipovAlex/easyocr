package com.antipov.easyocr.ui.activity.text_view;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 13.10.2017.
 */

interface TextViewPresenter extends BasePresenter {
    void updateText(long id, String text);

    void getImage(long id);
}

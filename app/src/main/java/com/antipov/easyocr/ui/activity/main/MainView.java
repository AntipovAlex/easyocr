package com.antipov.easyocr.ui.activity.main;

import com.antipov.easyocr.model.realm.Document;

import io.realm.RealmResults;

/**
 * Created by alex on 08.10.2017.
 */

public interface MainView{
    interface OnLoadDocuments {
        void onLoadDocumentsResult(RealmResults<Document> model);

        void onLoadDocumentsError(String error);
    }

    interface OnLoadDocument {
        void onLoadDocumentResult(Document model);

        void onLoadDocumentError(String error);
    }
}

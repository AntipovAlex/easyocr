package com.antipov.easyocr.ui.activity.document;

import android.content.Context;
import android.net.Uri;

import com.antipov.easyocr.R;
import com.antipov.easyocr.api.EasyOCRAPI;
import com.antipov.easyocr.model.OCRResponseModel;
import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.model.realm.FileImage;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.RetrofitUtils;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by alex on 11.10.2017.
 */

public class DocumentInteractorImpl implements DocumentInteractor {
    @Override
    public void loadFiles(long id, final OnLoadDocuments listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            final RealmResults<DocumentFile> documents = realm.where(DocumentFile.class).equalTo("documentId", id).findAllAsync();
            documents.addChangeListener(new RealmChangeListener<RealmResults<DocumentFile>>() {
                @Override
                public void onChange(RealmResults<DocumentFile> files) {
                    listener.onLoadDocumentsResult(files);
                    documents.removeAllChangeListeners();
                }
            });
        } catch (Exception e){
            listener.onLoadDocumentsError(e.getMessage());
        }
    }

    @Override
    public void doOCR(final Context context, final Uri uri, String languageCode, final OnOCR onOCRListener, final OnNoInternet onNoInternet) {
        File file = new File(uri.getPath());
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        final Observable<OCRResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(EasyOCRAPI.class).upload(body, languageCode);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Const.RETRY_COUNT)
                .subscribe(new Subscriber<OCRResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onOCRListener.onOCRError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(OCRResponseModel model) {
                        if (model.isSuccessfull()){
                            onOCRListener.onOCRResult(model.getData(), uri);
                        } else {
                            onOCRListener.onOCRError(context.getString(R.string.error_ocr));
                        }
                    }
                });
    }

    @Override
    public void saveFile(final long documentId, String thumb, String original, final OnSaveDocument listener, String text) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            final DocumentFile documentFile = new DocumentFile(text, thumb);
            final FileImage fileImage = new FileImage(original);

            final int[] nextIdFile = new int[1];
            final int[] nextIdImage = new int[1];

            // make in single transaction, because image cannot exist without file
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    // primary key AI implementation
                    // get id for file
                    Number id = realm.where(DocumentFile.class).max("id");
                    nextIdFile[0] = (id != null) ? id.intValue() + 1 : 0;
                    // set PK for file
                    documentFile.setId(nextIdFile[0]);
                    // get id for image
                    id = realm.where(FileImage.class).max("id");
                    nextIdImage[0] = (id != null) ? id.intValue() + 1 : 0;
                    // set PK for image
                    fileImage.setId(nextIdImage[0]);
                    // set FKs
                    documentFile.setImageId(fileImage.getId());
                    documentFile.setDocumentId(documentId);
                    // saving objects
                    realm.copyToRealm(fileImage);
                    realm.copyToRealm(documentFile);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onSaveDocumentResult(documentFile);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onSaveDocumentError(error.getMessage());
                }
            });
        } catch (Exception e){
            listener.onSaveDocumentError(e.getMessage());
        }
    }

    @Override
    public void deleteFile(final long id, long imageId, final String image, final OnDeleteDocument listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            final String[] originalImage = new String[1];
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    FileImage fileImage = realm.where(FileImage.class).equalTo("id", id).findFirst();
                    originalImage[0] = fileImage.getFilePath();
                    DocumentFile file = realm.where(DocumentFile.class).equalTo("id", id).findFirst();
                    file.deleteFromRealm();
                    fileImage.deleteFromRealm();
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onDeleteDocumentResult(id, image, originalImage[0]);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onDeleteDocumentError(error.getMessage());
                }
            });
        } catch (Exception e){
            listener.onDeleteDocumentError(e.getMessage());
        }
    }
}

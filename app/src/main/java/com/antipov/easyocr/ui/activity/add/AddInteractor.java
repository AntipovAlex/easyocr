package com.antipov.easyocr.ui.activity.add;

import android.net.Uri;

/**
 * Created by alex on 09.10.2017.
 */

public interface AddInteractor {
    void createDocument(String description, String title, Uri uri, OnCreateDocument onCreateDocumentListener);

    interface OnCreateDocument {
        void onCreateDocumentResult(int id);

        void onCreateDocumentError(String error);
    }
}

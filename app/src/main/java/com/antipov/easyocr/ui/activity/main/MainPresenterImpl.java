package com.antipov.easyocr.ui.activity.main;

import com.antipov.easyocr.model.realm.Document;

import io.realm.RealmResults;

/**
 * Created by alex on 08.10.2017.
 */

public class MainPresenterImpl implements MainPresenter, MainInteractor.OnLoadDocuments, MainInteractor.OnLoadDocument, MainInteractor.OnNoInternet {
    private final MainInteractorImpl mInteractor;
    private MainActivity mView;

    public MainPresenterImpl(MainActivity view) {
        mView = view;
        mInteractor = new MainInteractorImpl();
    }


    @Override
    public void loadDocuments() {
        mInteractor.loadDocuments(this);
    }

    @Override
    public void loadDocument(long id) {
        mInteractor.loadDocument(id, this);
    }

    @Override
    public void onLoadDocumentsResult(RealmResults<Document> model) {
        if (mView != null){
            mView.onLoadDocumentsResult(model);
        }
    }

    @Override
    public void onLoadDocumentsError(String error) {
        if (mView != null){
            mView.onLoadDocumentsError(error);
        }
    }


    @Override
    public void onLoadDocumentResult(Document model) {
        if (mView != null){
            mView.onLoadDocumentResult(model);
        }
    }

    @Override
    public void onLoadDocumentError(String error) {
        if (mView != null){
            mView.onLoadDocumentError(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

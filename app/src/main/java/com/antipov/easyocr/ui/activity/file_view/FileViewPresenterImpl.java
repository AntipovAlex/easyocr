package com.antipov.easyocr.ui.activity.file_view;

import com.antipov.easyocr.model.realm.FileImage;

/**
 * Created by alex on 13.10.2017.
 */

public class FileViewPresenterImpl implements FileViewPresenter, FileViewInteractor.OnGetFile, FileViewInteractor.OnNoInternet {
    private final FileViewInteractorImpl mInteractor;
    private FileViewActivity mView;

    public FileViewPresenterImpl(FileViewActivity view) {
        this.mView = view;
        this.mInteractor = new FileViewInteractorImpl();
    }

    @Override
    public void getImage(long id) {
        mInteractor.getFile(id, this);
    }

    @Override
    public void onGetFileResult(FileImage model) {
        if (mView != null){
            mView.onGetFileResult(model);
        }
    }

    @Override
    public void onGetFileError(String error) {
        if (mView != null){
            mView.onGetFileError(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

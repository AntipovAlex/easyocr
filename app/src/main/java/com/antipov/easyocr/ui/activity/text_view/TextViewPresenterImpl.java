package com.antipov.easyocr.ui.activity.text_view;

import android.widget.TextView;

import com.antipov.easyocr.model.realm.FileImage;

/**
 * Created by alex on 13.10.2017.
 */

public class TextViewPresenterImpl implements TextViewPresenter, TextViewInteractor.OnUpdateText,
        TextViewInteractor.OnGetFile, TextViewInteractor.OnNoInternet{
    private final TextViewInteractorImpl mInteractor;
    private TextViewActivity mView;

    public TextViewPresenterImpl(TextViewActivity view) {
        this.mView = view;
        this.mInteractor = new TextViewInteractorImpl();
    }

    @Override
    public void updateText(long id, String text) {
        mInteractor.updateText(id, text, this);
    }

    @Override
    public void getImage(long id) {
        mInteractor.getFile(id, this);
    }

    @Override
    public void onGetFileResult(FileImage model) {
        if (mView != null){
            mView.onGetFileResult(model);
        }
    }

    @Override
    public void onGetFileError(String error) {
        if (mView != null){
            mView.onGetFileError(error);
        }
    }

    @Override
    public void onUpdateTextResult(String text) {
        if (mView != null){
            mView.onUpdateTextResult(text);
        }
    }

    @Override
    public void onUpdateTextError(String error) {
        if (mView != null){
            mView.onUpdateTextError(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

package com.antipov.easyocr.ui.activity.document;

import android.net.Uri;

import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.ui.activity.base.BaseView;

import io.realm.RealmResults;

/**
 * Created by alex on 11.10.2017.
 */

public interface DocumentView extends BaseView {
    void onDeleteClicked(long id, long imageId, String image);

    void onShareClicked(String text);

    void onImageClicked(long id);

    void onTextClicked(String text, long id);

    interface OnLoadDocuments {
        void onLoadDocumentsResult(RealmResults<DocumentFile> model);

        void onLoadDocumentsError(String error);
    }

    interface OnOCR {
        void onOCRResult(String text, Uri uri);

        void onOCRError(String error);
    }

    interface OnSaveDocument {
        void onSaveDocumentResult(DocumentFile model);

        void onSaveDocumentError(String error);
    }

    interface OnDeleteDocument {
        void onDeleteDocumentResult(long id, String image, String original);

        void onDeleteDocumentError(String error);
    }
}

package com.antipov.easyocr.ui.activity.about;

import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.antipov.easyocr.R;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.ui.adapter.AboutPagerAdapter;
import com.rd.PageIndicatorView;

public class AboutActivity extends BaseActivity {

    private ViewPager viewPager;
    private PageIndicatorView pageIndicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void getExtras() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    protected void initViews() {
        viewPager = findViewById(R.id.about_pager);
        viewPager.setAdapter(new AboutPagerAdapter(getSupportFragmentManager()));
        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setViewPager(viewPager);
    }

    @Override
    protected void initListeners() {

    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initPresenter() {

    }


    @Override
    public void onNoInternet() {

    }
}

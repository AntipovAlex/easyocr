package com.antipov.easyocr.ui.activity.detail_document;

/**
 * Created by alex on 15.10.2017.
 */

public class DetailDocumentPresenterImpl implements DetailDocumentPresenter, DetailDocumentInteractor.OnGetInfo,
        DetailDocumentInteractor.OnDeleteProject, DetailDocumentInteractor.OnNoInternet {
    private DetailDocumentActivity mView;
    private final DetailDocumentInteractorImpl mInteractor;

    public DetailDocumentPresenterImpl(DetailDocumentActivity view) {
        this.mView = view;
        this.mInteractor = new DetailDocumentInteractorImpl();
    }

    @Override
    public void getInfo(long id) {
        mInteractor.getInfo(id, this, this);
    }

    @Override
    public void deleteProject(long id) {
        mInteractor.deleteProject(id, this);
    }

    @Override
    public void onGetInfoResult(int documentCount, String createdAt, String description, String name) {
        if (mView != null){
            mView.onGetInfoResult(documentCount, createdAt, description, name);
        }
    }

    @Override
    public void onGetInfoError(String error) {
        if (mView != null){
            mView.onGetInfoError(error);
        }
    }

    @Override
    public void onDeleteProjectResult(long id) {
        if (mView != null){
            mView.onDeleteProjectResult(id);
        }
    }

    @Override
    public void onDeleteProjectError(String error) {
        if (mView != null){
            mView.onDeleteProjectError(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

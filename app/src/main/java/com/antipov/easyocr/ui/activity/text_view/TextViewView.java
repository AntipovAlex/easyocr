package com.antipov.easyocr.ui.activity.text_view;

import com.antipov.easyocr.model.realm.FileImage;
import com.antipov.easyocr.ui.activity.base.BaseView;

/**
 * Created by alex on 13.10.2017.
 */

public interface TextViewView extends BaseView {
    interface OnUpdateText {
        void onUpdateTextResult(String text);

        void onUpdateTextError(String error);
    }

    interface OnGetFile {
        void onGetFileResult(FileImage model);

        void onGetFileError(String error);
    }
}

package com.antipov.easyocr.ui.activity.file_view;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 13.10.2017.
 */

public interface FileViewPresenter extends BasePresenter{

    void getImage(long id);
}

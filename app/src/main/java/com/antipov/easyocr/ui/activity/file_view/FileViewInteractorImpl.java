package com.antipov.easyocr.ui.activity.file_view;

import com.antipov.easyocr.model.realm.FileImage;
import io.realm.Realm;
import io.realm.RealmChangeListener;

/**
 * Created by alex on 13.10.2017.
 */

public class FileViewInteractorImpl implements FileViewInteractor {
    @Override
    public void getFile(long id, final OnGetFile listener) {
        try{
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            final FileImage documents = realm.where(FileImage.class).equalTo("id", id).findFirstAsync();
            documents.addChangeListener(new RealmChangeListener<FileImage>() {
                @Override
                public void onChange(FileImage files) {
                    listener.onGetFileResult(files);
                    documents.removeAllChangeListeners();
                }
            });
        } catch (Exception e){
            listener.onGetFileError(e.getMessage());
        }
    }
}

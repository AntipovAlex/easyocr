package com.antipov.easyocr.ui.activity.document;

import android.content.Context;
import android.net.Uri;

import com.antipov.easyocr.model.realm.DocumentFile;

import io.realm.RealmResults;

/**
 * Created by alex on 11.10.2017.
 */

public interface DocumentInteractor {

    void loadFiles(long id, OnLoadDocuments onLoadDocuments);

    void doOCR(Context context, Uri uri, String languageCode, OnOCR onOCRListener, OnNoInternet onNoInternet);

    void saveFile(long documentId, String thumb, String original, OnSaveDocument listener, String text);

    void deleteFile(long id, long imageId, String image, OnDeleteDocument listener);

    interface OnLoadDocuments {
        void onLoadDocumentsResult(RealmResults<DocumentFile> model);

        void onLoadDocumentsError(String error);
    }

    interface OnOCR {
        void onOCRResult(String text, Uri uri);

        void onOCRError(String error);
    }

    interface OnSaveDocument {
        void onSaveDocumentResult(DocumentFile model);

        void onSaveDocumentError(String error);
    }

    interface OnDeleteDocument {
        void onDeleteDocumentResult(long id, String image, String original);

        void onDeleteDocumentError(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}

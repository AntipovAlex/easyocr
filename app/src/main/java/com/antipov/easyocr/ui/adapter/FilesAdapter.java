package com.antipov.easyocr.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.antipov.easyocr.R;
import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.ui.activity.document.DocumentView;
import com.antipov.easyocr.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by alex on 11.10.2017.
 */

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder>{
    private final DocumentView listener;
    private final Context context;
    ArrayList<DocumentFile> data;

    public FilesAdapter(ArrayList<DocumentFile> data, Context context, DocumentView listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_file, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mText.setText(data.get(position).getText());
        holder.mText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTextClicked(data.get(position).getText(), data.get(position).getId());
            }
        });
        holder.mPreview.setImageBitmap(ImageUtils.getFromInternalStorage(data.get(position).getImage()));
        holder.mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImageClicked(data.get(position).getImageId());
            }
        });
        holder.mOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, v);
                //inflating menu from xml resource
                popup.inflate(R.menu.file_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.share:
                                listener.onShareClicked(data.get(position).getText());
                                break;
                            case R.id.delete:
                                listener.onDeleteClicked(data.get(position).getId(), data.get(position).getImageId(), data.get(position).getImage());
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(DocumentFile model) {
        data.add(model);
        notifyDataSetChanged();
    }

    public void update(long id, String text) {
        for (DocumentFile document: data){
            if (document.getId() == id){
                document.setText(text);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void delete(long id) {
        for (DocumentFile documentFile: data){
            if (documentFile.getId() == id){
                data.remove(documentFile);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mPreview;
        TextView mText;
        ImageView mOptions;
        public ViewHolder(View itemView) {
            super(itemView);
            mPreview = itemView.findViewById(R.id.iv_preview);
            mText = itemView.findViewById(R.id.tv_text);
            mOptions = itemView.findViewById(R.id.iv_share);
        }
    }
}

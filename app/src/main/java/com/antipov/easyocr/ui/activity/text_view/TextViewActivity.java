package com.antipov.easyocr.ui.activity.text_view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.antipov.easyocr.R;
import com.antipov.easyocr.model.realm.FileImage;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.ui.activity.file_view.FileViewActivity;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;
import com.antipov.easyocr.utils.ImageUtils;

public class TextViewActivity extends BaseActivity implements TextViewView,
        TextViewView.OnUpdateText, TextViewView.OnGetFile, View.OnClickListener {

    private Toolbar toolbar;
    private String text;
    private String name;
    private TextView mText;
    private ImageView mPhoto;
    private Menu mMenu;
    private TextViewPresenterImpl mPresenter;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgress();
        mPresenter.getImage(id);
    }

    @Override
    protected void getExtras() {
        text = getIntent().getExtras().getString(Const.Intent.TEXT);
        name = getIntent().getExtras().getString(Const.Intent.PROJECT_NAME);
        id = getIntent().getExtras().getLong(Const.Intent.FILE_ID);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_text_view;
    }

    @Override
    protected void initViews() {
        toolbar = findViewById(R.id.toolbar);
        mText = findViewById(R.id.et_text);
        mText.setText(text);
        mPhoto = findViewById(R.id.iv_photo);
//        collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
//        appBarLayout = findViewById(R.id.app_bar);
        // enabling icon "save" when tet edited
        mText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mMenu.setGroupEnabled(R.id.save, true);
                mMenu.setGroupVisible(R.id.save, true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void initListeners() {
        mPhoto.setOnClickListener(this);
//        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            boolean isShow = false;
//            int scrollRange = -1;
//
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (scrollRange == -1) {
//                    scrollRange = appBarLayout.getTotalScrollRange();
//                }
//                if (scrollRange + verticalOffset == 0) {
//                    collapsingToolbarLayout.setTitle("Title");
//                    isShow = true;
//                } else if(isShow) {
//                    collapsingToolbarLayout.setTitle(" ");
//                    isShow = false;
//                }
//            }
//        });
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(getString(R.string.in_document, name));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        mMenu = menu;
        mMenu.setGroupEnabled(R.id.save, false);
        mMenu.setGroupVisible(R.id.save, false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.toolbar_save:
                showProgress();
                mPresenter.updateText(id, mText.getText().toString());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_photo:
                Intent intent = new Intent(this, FileViewActivity.class);
                intent.putExtra(Const.Intent.PROJECT_ID, id);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void initPresenter() {
        mPresenter = new TextViewPresenterImpl(this);
    }

    @Override
    public void onUpdateTextResult(String text) {
        hideProgress();
        Intent intent = new Intent();
        intent.putExtra(Const.Intent.TEXT, text);
        intent.putExtra(Const.Intent.FILE_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onUpdateTextError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, error);
    }

    @Override
    public void onGetFileResult(FileImage model) {
        hideProgress();
        mPhoto.setImageBitmap(ImageUtils.getFromInternalStorage(model.getFilePath()));
    }

    @Override
    public void onGetFileError(String error) {
        hideProgress();
    }





    @Override
    public void onNoInternet() {

    }
}

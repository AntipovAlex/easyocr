package com.antipov.easyocr.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.antipov.easyocr.R;
import com.antipov.easyocr.interfaces.IOnDocumentClicked;
import com.antipov.easyocr.model.realm.Document;
import com.antipov.easyocr.ui.activity.detail_document.DetailDocumentActivity;
import com.antipov.easyocr.ui.activity.main.MainActivity;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by alex on 09.10.2017.
 */

public class
DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.ViewHolder> {
    private ArrayList<Document> data;
    private Activity context;
    private IOnDocumentClicked listener;

    public DocumentsAdapter(Activity context, ArrayList<Document> data, IOnDocumentClicked listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_project, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mTitle.setText(data.get(position).getName());
        holder.mDescription.setText(data.get(position).getDescription());
        holder.mCover.setImageBitmap(ImageUtils.getFromInternalStorage(data.get(position).getCoverPath()));
        holder.mCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDocumentClicked(data.get(position).getId(), data.get(position).getName());
            }
        });
        holder.mInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailDocumentActivity.class);
                intent.putExtra(Const.Intent.PROJECT_ID, data.get(position).getId());
                context.startActivityForResult(intent, MainActivity.DELETE_REQUEST);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(Document model) {
        data.add(model);
        notifyDataSetChanged();
    }

    public void delete(long id) {
        for (Document document: data){
            if (document.getId() == id){
                data.remove(document);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mDescription;
        ImageView mCover;
        ImageView mInfo;
        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.tv_title);
            mDescription = itemView.findViewById(R.id.tv_description);
            mCover = itemView.findViewById(R.id.iv_cover);
            mInfo = itemView.findViewById(R.id.iv_info);
        }
    }
}

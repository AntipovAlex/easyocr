package com.antipov.easyocr.ui.activity.document;

import android.content.Context;
import android.net.Uri;

import com.antipov.easyocr.model.realm.DocumentFile;

import io.realm.RealmResults;

/**
 * Created by alex on 11.10.2017.
 */

public class DocumentPresenterImpl implements DocumentPresenter, DocumentInteractor.OnLoadDocuments,
        DocumentInteractor.OnOCR, DocumentInteractor.OnSaveDocument, DocumentInteractor.OnNoInternet, DocumentInteractor.OnDeleteDocument {
    private final DocumentInteractorImpl mInteractor;
    private DocumentActivity mView;

    public DocumentPresenterImpl(DocumentActivity view) {
        this.mView = view;
        this.mInteractor = new DocumentInteractorImpl();
    }

    @Override
    public void loadFiles(long id) {
        mInteractor.loadFiles(id, this);
    }

    @Override
    public void doOCR(Context context, Uri uri, String languageCode) {
        mInteractor.doOCR(context, uri, languageCode, this, this);
    }

    @Override
    public void saveFile(long id, String thumb, String original, String text) {
        mInteractor.saveFile(id, thumb, original, this, text);
    }

    @Override
    public void deleteFile(long id, long imageId, String image) {
        mInteractor.deleteFile(id, imageId, image, this);
    }

    @Override
    public void onLoadDocumentsResult(RealmResults<DocumentFile> model) {
        if (mView != null){
            mView.onLoadDocumentsResult(model);
        }
    }

    @Override
    public void onLoadDocumentsError(String error) {
        if (mView != null){
            mView.onLoadDocumentsError(error);
        }
    }

    @Override
    public void onOCRResult(String text, Uri uri) {
        if (mView != null){
            mView.onOCRResult(text, uri);
        }
    }

    @Override
    public void onOCRError(String error) {
        if (mView != null){
            mView.onOCRError(error);
        }
    }

    @Override
    public void onSaveDocumentResult(DocumentFile model) {
        if (mView != null){
            mView.onSaveDocumentResult(model);
        }
    }

    @Override
    public void onSaveDocumentError(String error) {
        if (mView != null){
            mView.onSaveDocumentError(error);
        }
    }

    @Override
    public void onDeleteDocumentResult(long id, String image, String original) {
        if (mView != null) {
            mView.onDeleteDocumentResult(id, image, original);
        }
    }

    @Override
    public void onDeleteDocumentError(String error) {
        if (mView != null) {
            mView.onDeleteDocumentError(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

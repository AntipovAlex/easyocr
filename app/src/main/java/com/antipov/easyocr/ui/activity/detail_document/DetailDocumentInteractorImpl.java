package com.antipov.easyocr.ui.activity.detail_document;

import com.antipov.easyocr.model.realm.Document;
import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.model.realm.FileImage;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by alex on 15.10.2017.
 */

public class DetailDocumentInteractorImpl implements DetailDocumentInteractor {
    @Override
    public void getInfo(final long id, final OnGetInfo listener, OnNoInternet onNoInternet) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());

            final int[] documentCount = new int[1];
            final String[] createdAt = new String[1];
            final String[] description = new String[1];
            final String[] name = new String[1];

            // make in single transaction
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    documentCount[0] = realm.where(DocumentFile.class).equalTo("documentId", id).findAll().size();
                    createdAt[0] = realm.where(Document.class).equalTo("id", id).findFirst().getDateCreate();
                    description[0] = realm.where(Document.class).equalTo("id", id).findFirst().getDescription();
                    name[0] = realm.where(Document.class).equalTo("id", id).findFirst().getName();
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onGetInfoResult(documentCount[0], createdAt[0], description[0], name[0]);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onGetInfoError(error.getMessage());
                }
            });
        } catch (Exception e){
            listener.onGetInfoError(e.getMessage());
        }
    }

    @Override
    public void deleteProject(final long id, final OnDeleteProject listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());

            // make in single transaction
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    // no cascade delete in realm :(
                    Document document = realm.where(Document.class).equalTo("id", id).findFirst();
                    RealmResults<DocumentFile> documentFiles = realm.where(DocumentFile.class).equalTo("documentId", id).findAll();
                    for (DocumentFile documentFile: documentFiles){
                        realm.where(FileImage.class).equalTo("id", documentFile.getImageId()).findFirst().deleteFromRealm();
                    }
                    documentFiles.deleteAllFromRealm();
                    document.deleteFromRealm();

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onDeleteProjectResult(id);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onDeleteProjectError(error.getMessage());
                }
            });
        } catch (Exception e){
            listener.onDeleteProjectError(e.getMessage());
        }
    }
}

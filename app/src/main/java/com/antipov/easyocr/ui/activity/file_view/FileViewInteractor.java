package com.antipov.easyocr.ui.activity.file_view;

import com.antipov.easyocr.model.realm.FileImage;

/**
 * Created by alex on 13.10.2017.
 */

interface FileViewInteractor {

    void getFile(long id, OnGetFile listener);

    interface OnGetFile {
        void onGetFileResult(FileImage model);

        void onGetFileError(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}

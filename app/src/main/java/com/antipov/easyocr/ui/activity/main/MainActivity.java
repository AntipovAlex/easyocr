package com.antipov.easyocr.ui.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.easyocr.R;
import com.antipov.easyocr.interfaces.IOnDocumentClicked;
import com.antipov.easyocr.model.realm.Document;
import com.antipov.easyocr.ui.activity.about.AboutActivity;
import com.antipov.easyocr.ui.activity.add.AddActivity;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.ui.activity.document.DocumentActivity;
import com.antipov.easyocr.ui.adapter.DocumentsAdapter;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;
import com.antipov.easyocr.utils.SystemManager;
import com.antipov.easyocr.utils.realm.RealmHelper;

import java.util.ArrayList;
import java.util.Collection;

import io.realm.RealmResults;

public class MainActivity extends BaseActivity implements View.OnClickListener, MainView,
        MainView.OnLoadDocuments, MainView.OnLoadDocument, IOnDocumentClicked {

    private MainPresenterImpl mPresenter;
    private RelativeLayout mEmptyLayout;
    private RelativeLayout mErrorLayout;
    private FloatingActionButton mFab;
    private Button mAddButton;
    private final int ADD_DOC = 100;
    private Toolbar toolbar;
    private RecyclerView mRecycler;
    private DocumentsAdapter mAdapter;
    private ArrayList<Document> mData = new ArrayList<>();
    private GridLayoutManager layoutManager;
    private long documentId;
    public final static int DELETE_REQUEST = 10;
    private SystemManager systemManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgress();
        mPresenter.loadDocuments();
        systemManager = new SystemManager(context);
        if (systemManager.isFisrstTime()){
            showHelp();
            systemManager.setBeforeLaunched();
        }
    }

    private void showHelp() {
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }

    @Override
    protected void getExtras() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        mEmptyLayout = findViewById(R.id.empty_state_layout);
        mErrorLayout = findViewById(R.id.error_layout);
        mFab = findViewById(R.id.floatingActionButton);
        mAddButton = mEmptyLayout.findViewById(R.id.btn_create);
        toolbar = findViewById(R.id.toolbar);
        layoutManager = new GridLayoutManager(MainActivity.this, 2);
        mRecycler = findViewById(R.id.rv_documents);
        mRecycler.setLayoutManager(layoutManager);
    }

    @Override
    protected void initListeners() {
        mFab.setOnClickListener(this);
        mAddButton.setOnClickListener(this);
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    mFab.hide();
                else if (dy < 0)
                    mFab.show();
            }
        });
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setSubtitle(R.string.project_list);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.help_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.toolbar_help:
                DialogUtils.createInformationDialog(this ,getString(R.string.help),
                        getString(R.string.help_documents_content), true, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new MainPresenterImpl(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoadDocumentsResult(RealmResults<Document> model) {
        hideProgress();
        mData.addAll((ArrayList<Document>) RealmHelper.extractList(model));
        mAdapter = new DocumentsAdapter(this, mData, this);
        mRecycler.setAdapter(mAdapter);
        // if empty state
        if (model.isEmpty()) {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadDocumentsError(String error) {
        hideProgress();
        mErrorLayout.setVisibility(View.VISIBLE);
        ((TextView) mErrorLayout.findViewById(R.id.tv_error_msg)).setText(error);
        // if error was happened during load documents - allow load list again
        (mErrorLayout.findViewById(R.id.btn_refresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                mErrorLayout.setVisibility(View.GONE);
                mPresenter.loadDocuments();
            }
        });
    }

    @Override
    public void onLoadDocumentResult(Document model) {
        hideProgress();
        mAdapter.add(model);
        // hiding empty state if visible
        if (mEmptyLayout.getVisibility() == View.VISIBLE){
            mEmptyLayout.setVisibility(View.GONE);
        }
        DialogUtils.showSnackbar(this, getString(R.string.document_created_successfully));
    }

    @Override
    public void onLoadDocumentError(String error) {
        hideProgress();
        mErrorLayout.setVisibility(View.VISIBLE);
        ((TextView) mErrorLayout.findViewById(R.id.tv_error_msg)).setText(error);
        // if error was happened during load specific document - allow load document again
        (mErrorLayout.findViewById(R.id.btn_refresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                mErrorLayout.setVisibility(View.GONE);
                mPresenter.loadDocument(documentId);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.floatingActionButton:
                startAddActivity();
                break;
            case R.id.btn_create:
                startAddActivity();
                break;
        }
    }

    private void startAddActivity() {
        Intent intent = new Intent(this, AddActivity.class);
        startActivityForResult(intent, ADD_DOC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ADD_DOC:
                if (resultCode == RESULT_OK){
                    showProgress();
                    documentId = data.getIntExtra(Const.Intent.PROJECT_ID, 0);
                    mPresenter.loadDocument(documentId);
                }
                break;
            case DELETE_REQUEST:
                if (resultCode == RESULT_OK){
                    mAdapter.delete(data.getLongExtra(Const.Intent.PROJECT_ID, 0));
                    if(mAdapter.getItemCount() == 0){
                        mEmptyLayout.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    @Override
    public void onDocumentClicked(long id, String name) {
        Intent intent = new Intent(this, DocumentActivity.class);
        intent.putExtra(Const.Intent.PROJECT_ID, id);
        intent.putExtra(Const.Intent.PROJECT_NAME, name);
        startActivity(intent);
    }

    @Override
    public void onNoInternet() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}

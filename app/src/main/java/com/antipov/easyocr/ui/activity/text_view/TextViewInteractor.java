package com.antipov.easyocr.ui.activity.text_view;

import com.antipov.easyocr.model.realm.FileImage;


/**
 * Created by alex on 13.10.2017.
 */

public interface TextViewInteractor {
    void updateText(long id, String text, OnUpdateText listener);

    void getFile(long id, OnGetFile listener);

    interface OnGetFile {
        void onGetFileResult(FileImage model);

        void onGetFileError(String error);
    }

    interface OnUpdateText {
        void onUpdateTextResult(String text);

        void onUpdateTextError(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}

package com.antipov.easyocr.ui.activity.document;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.easyocr.R;
import com.antipov.easyocr.interfaces.IOnBitmapSave;
import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.ui.activity.file_view.FileViewActivity;
import com.antipov.easyocr.ui.activity.text_view.TextViewActivity;
import com.antipov.easyocr.ui.adapter.FilesAdapter;
import com.antipov.easyocr.ui.decorator.RecyclerDecorator;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;
import com.antipov.easyocr.utils.ImageUtils;
import com.antipov.easyocr.utils.realm.PermUtils;
import com.antipov.easyocr.utils.realm.RealmHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.theartofdev.edmodo.cropper.CropImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import io.realm.RealmResults;

public class DocumentActivity extends BaseActivity implements DocumentView, DocumentView.OnLoadDocuments,
        View.OnClickListener, DocumentView.OnOCR, DocumentView.OnSaveDocument, DocumentView.OnDeleteDocument,
        IOnBitmapSave {

    private RelativeLayout mEmptyLayout;
    private RelativeLayout mErrorLayout;
    private Button mAddButton;
    private ImageView mImage;
    private Toolbar toolbar;
    private long id;
    private String name;
    private DocumentPresenterImpl mPresenter;
    private FloatingActionButton mFab;
    private ImageUtils imageUtils;
    private int firstRequest;
    private String text;
    private LinearLayoutManager layoutManager;
    private ArrayList<DocumentFile> mData = new ArrayList<>();
    private FilesAdapter mAdapter;
    private RecyclerView mRecycler;
    private final int REQUEST_TEXT_UPDATE = 20;
    private final int REQUEST_ALLOW_CAMERA = 21;
    private Button mTryAgain;
    private String languageCode;
    private String thumb;
    private String full;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUtils = new ImageUtils(this, this);
        showProgress();
        mPresenter.loadFiles(id);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void getExtras() {
        id = getIntent().getExtras().getLong(Const.Intent.PROJECT_ID);
        name = getIntent().getExtras().getString(Const.Intent.PROJECT_NAME);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_document;
    }

    @Override
    protected void initViews() {
        mRecycler = findViewById(R.id.rv_files);
        mEmptyLayout = findViewById(R.id.empty_state_layout);
        ((TextView)mEmptyLayout.findViewById(R.id.tv_empty)).setText(R.string.no_files);
        mErrorLayout = findViewById(R.id.error_layout);
        mImage = mEmptyLayout.findViewById(R.id.iv_empty);
        mImage.setImageResource(R.drawable.ic_insert_drive);
        mFab = findViewById(R.id.floatingActionButton);
        mAddButton = mEmptyLayout.findViewById(R.id.btn_create);
        mAddButton.setText(R.string.create_file);
        toolbar = findViewById(R.id.toolbar);
        layoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(layoutManager);
        mRecycler.addItemDecoration(new RecyclerDecorator(this));
        mTryAgain = mErrorLayout.findViewById(R.id.btn_refresh);
    }

    @Override
    protected void initListeners() {
        mFab.setOnClickListener(this);
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    mFab.hide();
                else if (dy < 0)
                    mFab.show();
            }
        });
        mAddButton.setOnClickListener(this);
        mTryAgain.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.files);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(getString(R.string.in_document, name));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.help_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.toolbar_help:
                DialogUtils.createInformationDialog(this,getString(R.string.help),
                        getString(R.string.help_document_content), true, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new DocumentPresenterImpl(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoadDocumentsResult(RealmResults<DocumentFile> model) {
        hideProgress();
        mData.addAll((ArrayList<DocumentFile>) RealmHelper.extractList(model));
        mAdapter = new FilesAdapter(mData,  context, this);
        mRecycler.setAdapter(mAdapter);
        if (model.isEmpty()){
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadDocumentsError(String error) {
        hideProgress();
        mErrorLayout.setVisibility(View.VISIBLE);
        ((TextView) mErrorLayout.findViewById(R.id.tv_error_msg)).setText(error);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.floatingActionButton:
                addFile();
                break;
            case R.id.btn_create:
                addFile();
                break;
            case R.id.btn_refresh:
                showProgress();
                mPresenter.loadFiles(id);
                break;
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PermUtils.checkIfAlreadyhavePermission(this)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_ALLOW_CAMERA);
            } else {
                imageUtils.pickFromCamera();
            }
        } else {
            imageUtils.pickFromCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ALLOW_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imageUtils.pickFromCamera();
                } else {
                    DialogUtils.createInformationDialog(this, getString(R.string.permisson_title), getString(R.string.permission_content),
                            true, null).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void addFile() {
        final MaterialDialog md = DialogUtils.createCustomDialog(this, getString(R.string.open_image),
                R.layout.choose_file_dialog).build();
        // set listeners
        md.findViewById(R.id.rb_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageCode = ((Spinner)md.findViewById(R.id.spinner)).getSelectedItem().toString();
                checkPermission();
                md.dismiss();
            }
        });
        md.findViewById(R.id.rb_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageCode = ((Spinner)md.findViewById(R.id.spinner)).getSelectedItem().toString();
                imageUtils.pickFromGallery();
                md.dismiss();
            }
        });
        // set spinner value by locale of the device
        ((Spinner)md.findViewById(R.id.spinner)).setSelection(Arrays.asList(getResources().getStringArray(R.array.spinnerItems)).indexOf(
                Locale.getDefault().getISO3Language()
        ));
        md.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ImageUtils.PICK_CAMERA:
                if (resultCode == RESULT_OK){
                    firstRequest = requestCode;
                    CropImage.activity(imageUtils.getImageUri())
                            .start(this);
                }
                break;
            case ImageUtils.PICK_GALLERY:
                if (resultCode == RESULT_OK){
                    firstRequest = requestCode;
                    CropImage.activity(data.getData())
                            .start(this);
                }
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    imageUtils.deleteTempFile();
                    // when image cropped send it to server for OCR
                    showProgress();
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mPresenter.doOCR(context, result.getUri(), languageCode);
                }
                break;
            case REQUEST_TEXT_UPDATE:
                if (resultCode == RESULT_OK){
                    mAdapter.update(
                            data.getExtras().getLong(Const.Intent.FILE_ID),
                            data.getExtras().getString(Const.Intent.TEXT));
                    DialogUtils.showSnackbar(this, getString(R.string.file_edited_succesfully));
                }
                break;
        }
    }

    @Override
    public void onOCRResult(String text, Uri uri) {
        // if OCR successful trying to save image
        this.text = text;
        imageUtils.savePicture(uri, Const.Mode.THUMBANDFULL);
    }

    @Override
    public void onOCRError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.error_while_ocr, error));
    }

    @Override
    public void onBitmapSaveSuccess(String path) {
        // not used there
    }

    @Override
    public void onBitmapSaveThumbAndFullSuccess(String full, String thumb) {
        // saving for delete in case of error
        this.full = full;
        this.thumb = thumb;
        // if image saved, make object in db with file
        mPresenter.saveFile(id,thumb, full, text);
    }

    @Override
    public void onBitmapSaveError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.error_while_ocr, error));
    }

    @Override
    public void onSaveDocumentResult(DocumentFile model) {
        hideProgress();
        mAdapter.add(model);
        DialogUtils.showSnackbar(this, getString(R.string.file_processed_succesfully));
        // hiding empty state if visible
        if (mEmptyLayout.getVisibility() == View.VISIBLE){
            mEmptyLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveDocumentError(String error) {
        // if during saving file in db happened error, deleting images from system
        // because just images has no sense without file in db
        imageUtils.deleteSavedFile(full);
        imageUtils.deleteSavedFile(thumb);
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.error_while_saving_file, error));
    }

    @Override
    public void onImageClicked(long imageId) {
        Intent intent = new Intent(this, FileViewActivity.class);
        intent.putExtra(Const.Intent.PROJECT_ID, imageId);
        startActivity(intent);
    }

    @Override
    public void onTextClicked(String text, long id) {
        Intent intent = new Intent(this, TextViewActivity.class);
        intent.putExtra(Const.Intent.TEXT, text);
        intent.putExtra(Const.Intent.PROJECT_NAME, name);
        intent.putExtra(Const.Intent.FILE_ID, id);
        startActivityForResult(intent, REQUEST_TEXT_UPDATE);
    }

    @Override
    public void onShareClicked(String text) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getString(R.string.share_title, text);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_topic));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
    }

    @Override
    public void onDeleteClicked(final long id, final long imageId, final String image) {
        DialogUtils.createBasicDialog(this, R.string.delete, getString(R.string.are_you_sure_delete_file), true, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                showProgress();
                mPresenter.deleteFile(id, imageId, image);
            }
        }, getString(R.string.ok), getString(R.string.cancel)).show();
    }

    @Override
    public void onDeleteDocumentResult(long id, String image, String original) {
        hideProgress();
        mAdapter.delete(id);
        if (mAdapter.getItemCount() == 0){
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
        // delete images from FS
        imageUtils.deleteSavedFile(image);
        imageUtils.deleteSavedFile(original);
        DialogUtils.showSnackbar(this, getString(R.string.document_was_deleted));
    }

    @Override
    public void onDeleteDocumentError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.error_during_deleting_file));
    }

    @Override
    public void onNoInternet() {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.no_internet_connection));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}

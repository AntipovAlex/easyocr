package com.antipov.easyocr.ui.activity.add;

import android.net.Uri;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 09.10.2017.
 */

public interface AddPresenter extends BasePresenter {
    void createDocument(String s, String title, Uri uri);
}

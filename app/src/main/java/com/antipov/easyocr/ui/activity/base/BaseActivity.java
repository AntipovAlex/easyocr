package com.antipov.easyocr.ui.activity.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.easyocr.R;


/**
 * Created by alex on 08.10.2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    public Context context;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        getExtras();
        initContext();
        initSessionManager();
        initPresenter();
        initViews();
        initToolbar();
        initListeners();
    }

    protected abstract void getExtras();

    protected abstract int getLayoutId();

    protected abstract void initViews();

    protected abstract void initListeners();

    protected abstract void initToolbar();

    protected abstract void initPresenter();

    private void initSessionManager(){

    }

    private void initContext() {
        context = getApplicationContext();
    }

    public void showProgress(){
        materialDialog = new MaterialDialog.Builder(this)
                .title(R.string.progress_dialog_title)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    public void hideProgress(){
        if (materialDialog != null){
            materialDialog.dismiss();
        }
    }

    public void hideSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


package com.antipov.easyocr.ui.activity.main;

import com.antipov.easyocr.model.realm.Document;

import io.realm.RealmResults;

/**
 * Created by alex on 08.10.2017.
 */

interface MainInteractor {
    void loadDocuments(OnLoadDocuments onLoadDocumentsListener);

    void loadDocument(long id, OnLoadDocument onLoadDocumentsListener);

    interface OnLoadDocuments {
        void onLoadDocumentsResult(RealmResults<Document> model);

        void onLoadDocumentsError(String error);
    }

    interface OnLoadDocument {
        void onLoadDocumentResult(Document model);

        void onLoadDocumentError(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}

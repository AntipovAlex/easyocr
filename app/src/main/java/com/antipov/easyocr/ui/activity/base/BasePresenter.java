package com.antipov.easyocr.ui.activity.base;

/**
 * Created by Antipov on 21.08.2017.
 */

public interface BasePresenter {
    void onDestroy();
}

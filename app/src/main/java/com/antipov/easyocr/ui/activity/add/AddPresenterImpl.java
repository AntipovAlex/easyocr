package com.antipov.easyocr.ui.activity.add;

import android.net.Uri;

/**
 * Created by alex on 09.10.2017.
 */

public class AddPresenterImpl implements AddPresenter, AddInteractor.OnCreateDocument {

    private AddInteractorImpl mInteractor;
    private AddActivity mView;

    public AddPresenterImpl(AddActivity view) {
        mView = view;
        mInteractor = new AddInteractorImpl();
    }

    @Override
    public void createDocument(String title, String description, Uri uri) {
        mInteractor.createDocument(title, description, uri,this);
    }

    @Override
    public void onCreateDocumentResult(int id) {
        if (mView != null){
            mView.onCreateDocumentResult(id);
        }
    }

    @Override
    public void onCreateDocumentError(String error) {
        if (mView != null){
            mView.onCreateDocumentError(error);
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}

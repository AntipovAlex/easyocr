package com.antipov.easyocr.ui.activity.text_view;

import com.antipov.easyocr.model.realm.DocumentFile;
import com.antipov.easyocr.model.realm.FileImage;

import io.realm.Realm;
import io.realm.RealmChangeListener;

/**
 * Created by alex on 13.10.2017.
 */

public class TextViewInteractorImpl implements TextViewInteractor {
    @Override
    public void updateText(final long id, final String text, final OnUpdateText listener) {
        try{
            final Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            // start transaction for update document
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(final Realm realm) {
                    // find document with id
                    final DocumentFile file = realm.where(DocumentFile.class).equalTo("id", id).findFirst();
                    // set new text to document
                    file.setText(text);
                    realm.copyToRealmOrUpdate(file);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onUpdateTextResult(text);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onUpdateTextError(error.getMessage());
                }
            });
        } catch (Exception e){
            listener.onUpdateTextError(e.getMessage());
        }
    }

    @Override
    public void getFile(long id, final OnGetFile listener) {
        try {
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            final FileImage documents = realm.where(FileImage.class).equalTo("id", id).findFirstAsync();
            documents.addChangeListener(new RealmChangeListener<FileImage>() {
                @Override
                public void onChange(FileImage files) {
                    listener.onGetFileResult(files);
                    documents.removeAllChangeListeners();
                }
            });
        } catch (Exception e) {
            listener.onGetFileError(e.getMessage());

        }
    }
}

package com.antipov.easyocr.ui.activity.file_view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.antipov.easyocr.R;
import com.antipov.easyocr.model.realm.FileImage;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;
import com.antipov.easyocr.utils.ImageUtils;

public class FileViewActivity extends BaseActivity implements View.OnClickListener, FileViewView, FileViewView.OnGetFile {

    private long id;
    private LinearLayout mBackground;
    private FileViewPresenterImpl mPresenter;
    private ImageView mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgress();
        mPresenter.getImage(id);
    }

    @Override
    protected void getExtras() {
        id = getIntent().getExtras().getLong(Const.Intent.PROJECT_ID);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_file_view;
    }

    @Override
    protected void initViews() {
        mBackground = findViewById(R.id.ll_background);
        mImage = findViewById(R.id.iv_image);
    }

    @Override
    protected void initListeners() {
        mBackground.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initPresenter() {
        mPresenter = new FileViewPresenterImpl(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_background:
                finish();
                break;
        }
    }

    @Override
    public void onGetFileResult(FileImage model) {
        hideProgress();
        mImage.setImageBitmap(ImageUtils.getFromInternalStorage(model.getFilePath()));
    }

    @Override
    public void onGetFileError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, error);
    }

    @Override
    public void onNoInternet() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}

package com.antipov.easyocr.ui.activity.file_view;

import com.antipov.easyocr.model.realm.FileImage;
import com.antipov.easyocr.ui.activity.base.BaseView;

/**
 * Created by alex on 13.10.2017.
 */

public interface FileViewView extends BaseView {
    interface OnGetFile {
        void onGetFileResult(FileImage model);

        void onGetFileError(String error);
    }
}

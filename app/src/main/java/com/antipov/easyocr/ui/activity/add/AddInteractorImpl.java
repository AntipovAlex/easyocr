package com.antipov.easyocr.ui.activity.add;

import android.net.Uri;

import com.antipov.easyocr.model.realm.Document;
import com.antipov.easyocr.model.realm.DocumentFile;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by alex on 09.10.2017.
 */

public class AddInteractorImpl implements AddInteractor {

    @Override
    public void createDocument(String title, String description, Uri uri, final OnCreateDocument onCreateDocumentListener) {
        try {
            final Document p = new Document(
                    title, description, uri.getPath(), new RealmList<DocumentFile>()
            );

            final int[] nextId = new int[1];
            Realm realm = Realm.getInstance(Realm.getDefaultConfiguration());
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    // primary key AI implementation
                    Number id = realm.where(Document.class).max("id");
                    nextId[0] = (id != null) ? id.intValue() + 1 : 0;
                    // persisting object
                    p.setId(nextId[0]);
                    realm.copyToRealm(p);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    onCreateDocumentListener.onCreateDocumentResult(nextId[0]);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    onCreateDocumentListener.onCreateDocumentError(error.getMessage());
                }
            });
        } catch (Exception e){
            onCreateDocumentListener.onCreateDocumentError(e.getMessage());
        }
    }
}

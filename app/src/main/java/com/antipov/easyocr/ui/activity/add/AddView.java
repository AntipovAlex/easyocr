package com.antipov.easyocr.ui.activity.add;

/**
 * Created by alex on 09.10.2017.
 */

public interface AddView {
    interface OnCreateDocument {
        void onCreateDocumentResult(int id);

        void onCreateDocumentError(String error);
    }
}

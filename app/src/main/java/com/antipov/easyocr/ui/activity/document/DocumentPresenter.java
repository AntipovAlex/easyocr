package com.antipov.easyocr.ui.activity.document;

import android.content.Context;
import android.net.Uri;

import com.antipov.easyocr.ui.activity.base.BasePresenter;

/**
 * Created by alex on 11.10.2017.
 */

public interface DocumentPresenter extends BasePresenter {
    void loadFiles(long id);

    void doOCR(Context context, Uri uri, String languageCode);

    void saveFile(long id, String thumb, String original, String text);

    void deleteFile(long id, long imageId, String image);
}

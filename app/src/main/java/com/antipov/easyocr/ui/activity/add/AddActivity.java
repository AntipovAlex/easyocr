package com.antipov.easyocr.ui.activity.add;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.easyocr.R;
import com.antipov.easyocr.interfaces.IOnBitmapSave;
import com.antipov.easyocr.ui.activity.base.BaseActivity;
import com.antipov.easyocr.utils.Const;
import com.antipov.easyocr.utils.DialogUtils;
import com.antipov.easyocr.utils.ImageUtils;
import com.antipov.easyocr.utils.realm.PermUtils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddActivity extends BaseActivity implements AddView, AddView.OnCreateDocument,
        View.OnClickListener, IOnBitmapSave {

    private Toolbar toolbar;
    private TextView mTitle;
    private Button mAddCover;
    private Menu mMenu;
    private AddPresenterImpl mPresenter;
    private TextView mDescription;
    private ImageUtils imageUtils;
    private Uri image;
    private TextView mFileCaption;
    private int firstRequest;
    private String path;
    private final int REQUEST_ALLOW_CAMERA = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUtils = new ImageUtils(this, this);
    }

    @Override
    protected void getExtras() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add;
    }

    @Override
    protected void initViews() {
        toolbar = findViewById(R.id.toolbar);
        mTitle = findViewById(R.id.tv_project_title);
        mDescription = findViewById(R.id.tv_description);
        mAddCover = findViewById(R.id.btn_open_cover);
        mFileCaption = findViewById(R.id.tv_file);
    }

    @Override
    protected void initListeners() {
        mAddCover.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(R.string.create_project);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new AddPresenterImpl(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.toolbar_save:
                if (validateFields()){
                    showProgress();
                    mPresenter.createDocument(mTitle.getText().toString(), mDescription.getText().toString(), image);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validateFields() {
        mTitle.setError(null);
        mDescription.setError(null);
        Matcher m = Pattern.compile(Const.Validation.REG_EXP_TITLE).matcher(mTitle.getText().toString());
        if (!m.matches()) {
            mTitle.setError(getString(R.string.validation_title, Integer.toString(Const.Validation.MIN_LENGHT_TITLE),
                    Integer.toString(Const.Validation.MAX_LENGHT_TITLE)));
            return false;
        }
        m = Pattern.compile(Const.Validation.REG_EXP_DESCR).matcher(mDescription.getText().toString());
        if (!m.matches()) {
            mDescription.setError(getString(R.string.validation_descr, Integer.toString(Const.Validation.MIN_LENGHT_TITLE),
                    Integer.toString(Const.Validation.MAX_LENGHT_TITLE)));
            return false;
        }
        if (image == null) {
            DialogUtils.showSnackbar(this, getString(R.string.validation_image));
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_open_cover:
                DialogUtils.createSingleChoiceItemsDialog(this, getString(R.string.open_image),
                        Arrays.asList(getString(R.string.from_camera), getString(R.string.from_gallery)), -1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                switch (which){
                                    case 0:
                                        checkPermission();
                                        break;
                                    case 1:
                                        imageUtils.pickFromGallery();
                                        break;
                                }
                                return false;
                            }
                        }).show();
                break;
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PermUtils.checkIfAlreadyhavePermission(this)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_ALLOW_CAMERA);
            } else {
                imageUtils.pickFromCamera();
            }
        } else {
            imageUtils.pickFromCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ALLOW_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imageUtils.pickFromCamera();
                } else {
                    DialogUtils.createInformationDialog(this, getString(R.string.permisson_title), getString(R.string.permission_content),
                            true, null).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtils.PICK_CAMERA ){ // if returned from camera
            if (resultCode == RESULT_OK){
                firstRequest = requestCode;
                CropImage.activity(imageUtils.getImageUri())
                        .start(this);
            }
        } else if (requestCode == ImageUtils.PICK_GALLERY){ // if returned from gallery
            if (resultCode == RESULT_OK){
                firstRequest = requestCode;
                CropImage.activity(data.getData())
                        .start(this);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){ // if returned from crop activity
            if (resultCode == RESULT_OK){
                imageUtils.deleteTempFile();
                // when image cropped saving the thumbnail
                showProgress();
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                image = result.getUri();
                imageUtils.savePicture(image, Const.Mode.THUMBNAIL);
            }
        }
    }

    @Override
    public void onCreateDocumentResult(int id) {
        // in case of success, returning to activity id of the created project
        // not returning project object because realm object is not serializable
        hideProgress();
        Intent intent = new Intent();
        intent.putExtra(Const.Intent.PROJECT_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCreateDocumentError(String error) {
        // no need delete thumbnial if was error while saving in db
        // because user always can try to save again.
        // Anyway, thumbnial deletes if user press "back" without saving
        hideProgress();
        DialogUtils.showSnackbar(this, error);
    }


    @Override
    public void onBitmapSaveSuccess(String path) {
        // saving for delete, if user leaves activity
        this.path = path;
        hideProgress();
        mFileCaption.setVisibility(View.VISIBLE);
        mAddCover.setText(R.string.change_file);
        DialogUtils.showSnackbar(this, getString(R.string.file_selected));
    }

    @Override
    public void onBitmapSaveThumbAndFullSuccess(String full, String thumb) {
        // not used there
    }

    @Override
    public void onBitmapSaveError(String error) {
        hideProgress();
        DialogUtils.showSnackbar(this, error);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (path != null){
            imageUtils.deleteSavedFile(path);
        }
    }

    @Override
    public void onNoInternet() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

}

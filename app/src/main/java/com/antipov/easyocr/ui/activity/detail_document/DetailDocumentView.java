package com.antipov.easyocr.ui.activity.detail_document;

import com.antipov.easyocr.ui.activity.base.BaseView;

/**
 * Created by alex on 15.10.2017.
 */

public interface DetailDocumentView extends BaseView {
    interface OnGetInfo {
        void onGetInfoResult(int documentCount, String createdAt, String description, String name);

        void onGetInfoError(String error);
    }

    interface OnDeleteProject {
        void onDeleteProjectResult(long id);

        void onDeleteProjectError(String error);
    }
}
